var gulp         = require('gulp'), // Gulp
    stylus       = require('gulp-stylus'), // Stylus
    autoprefixer = require('autoprefixer-stylus'), // Autoprefixer
    sourcemaps   = require('gulp-sourcemaps'), // Sourcemaps
    concat       = require('gulp-concat'), // Concat
    imagemin     = require('gulp-imagemin'), // Imagemin
    changed      = require('gulp-changed'), // Changed
    browsersync  = require('browser-sync'); // Browser-Sync


var dev_path =
    {
        styl: 'dev/styl/',
        html: 'dev/',
        js:   'dev/js/',
        fonts:'dev/fonts/',
        img:  'dev/img/'
    }


var build_path =
    {
        css:  'project/css/',
        html: 'project/',
        js:   'project/js/',
        fonts:'project/fonts/',
        img:  'project/img/'
    }





// Compile html
gulp.task('html', function(){
    gulp.src(dev_path.html + '*.html')
        .pipe(gulp.dest(build_path.html))
        .pipe(browsersync.reload({stream: true}));
});


// Compile Stylus
gulp.task('stylus', function(){
    gulp.src([
            dev_path.styl + '*.styl', 
            '!' + dev_path.styl + '_*.styl'
        ])
        .pipe(stylus({
            use: [autoprefixer()],
            //compress: true,
            sourcemap: {
                inline: true,
                sourceRoot: dev_path.styl
            }
        }))
        .on('error', console.log)
        .pipe(gulp.dest(build_path.css))
        .pipe(browsersync.reload({stream: true}));
});
 

// JavaScript
gulp.task('js', function(){
    gulp.src([
            dev_path.js + '**/*',
            '!' + dev_path.js + '_*.js'
        ])
        .on('error', console.log)
        .pipe(gulp.dest(build_path.js))
        .pipe(browsersync.reload({stream: true}));
});



// Minification Images
gulp.task('images', function(){
    gulp.src([dev_path.img + '**/*'])
        .pipe(changed(build_path.img))
        .pipe(imagemin())
        .pipe(gulp.dest(build_path.img))
        .pipe(browsersync.reload({stream: true}));
});


// Start Browser-Sync server
gulp.task('browsersync-server', function(){
    browsersync.init(null, {
        server: {baseDir: 'project/'},
        open: false,
        notify: false
    });
});





//
// TRANSFER VENDOR FILES
//

gulp.task('vendor', function(){
    gulp.src(dev_path.styl + 'vendor/*').pipe(gulp.dest(build_path.css));
    gulp.src(dev_path.js + 'vendor/*').pipe(gulp.dest(build_path.js));
    gulp.src(dev_path.fonts + '*').pipe(gulp.dest(build_path.fonts));
});





//
// WATCH TASK
//

gulp.task('watch', function(){
    gulp.watch(dev_path.html + '**/*.html', ['html']);
    gulp.watch(dev_path.styl + '**/*.styl', ['stylus']);
    gulp.watch(dev_path.js + '**/*.js', ['js']);
    gulp.watch([dev_path.img + '**/*'], ['images']);
    gulp.watch([dev_path.styl + 'vendor/*', dev_path.js + 'vendor/*']);
});




//
// DEFAULT TASK
//

gulp.task('default', [
    'vendor', 'stylus', 'js', 'browsersync-server', 'images', 'watch', 'html'
]);





